// owlCarousel slider
$(document).ready(function () {
  $(".slide-container").slick({
    prevArrow: false,
    nextArrow: false,
    // dots: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false,
        },
      },
    ],
  });
});
const menuToggle = document.querySelector(".header-icon-toggle");
const menuHeader = document.querySelector(".header-menu");
const expandClass = "is-expand";
menuToggle.addEventListener("click", () => {
  menuHeader.classList.add(expandClass);
});

window.addEventListener("click", (e) => {
  if (
    !menuHeader.contains(e.target) &&
    !e.target.matches(".header-icon-toggle")
  ) {
    menuHeader.classList.remove(expandClass);
  }
});
